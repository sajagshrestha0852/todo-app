import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Todo from "./Todo";
import Nav from "./Nav";
import Home from "./Home";
function App() {
  return (
    <Router>
      <div>
        <Nav />
        <div className="container">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/todo" component={Todo} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
