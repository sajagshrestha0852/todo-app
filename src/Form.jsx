import React, { useState } from "react";

const Form = ({ addTodo }) => {
  const [state, setState] = useState({
    title: "",
    body: ""
  });

  const submitHandler = e => {
    e.preventDefault();
    addTodo(state);
    setState({
      title: "",
      body: ""
    });
  };

  return (
    <form onSubmit={submitHandler}>
      <input
        type="text"
        className="form-control mb-3 mt-3"
        placeholder="enter title"
        value={state.title}
        onChange={e => {
          setState({ ...state, title: e.target.value });
        }}
      />
      <input
        type="text"
        className="form-control mb-3"
        placeholder="enter body"
        value={state.body}
        onChange={e => setState({ ...state, body: e.target.value })}
      />
      <button type="submit" className="btn btn-primary">
        submit
      </button>
    </form>
  );
};

export default Form;
