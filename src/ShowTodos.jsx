import React from "react";

const ShowTodos = ({ todo, delTodo, index }) => {
  return (
    <div className="card mt-5">
      <div className="card-header">{todo.title}</div>
      <div className="card-body">{todo.body}</div>
      <div className="card-body">
        <button className="btn btn-danger" onClick={() => delTodo(todo.title)}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default ShowTodos;
