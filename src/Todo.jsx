import React, { useReducer } from "react";
import ShowTodos from "./ShowTodos";
import Form from "./Form";

const initialState = [];

const reducer = (state, action) => {
  switch (action.type) {
    case "add":
      return [...state, action.payload];
    case "delete":
      return action.updatedState;
    default:
      return initialState;
  }
};

function Todo() {
  const [todos, dispatch] = useReducer(reducer, initialState);

  const addTodo = newTodo => {
    if (newTodo.title === "" || newTodo.body === "") {
      dispatch({
        type: "add",
        payload: { title: "empty title or body" }
      });
    } else {
      dispatch({ type: "add", payload: newTodo });
    }
  };

  const delTodo = todoTitle => {
    const newTodo = todos.filter(todo => {
      return todo.title !== todoTitle;
    });
    dispatch({ type: "delete", updatedState: newTodo });
  };

  return (
    <div>
      <h1>Todo App</h1>
      <Form addTodo={addTodo} />
      {todos.map((todo, index) => (
        <ShowTodos delTodo={delTodo} key={index} todo={todo} />
      ))}
    </div>
  );
}
export default Todo;
